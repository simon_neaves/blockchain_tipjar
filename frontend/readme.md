# Frontend
Contains all frontend related code for baseproject. Both es6 and JSX are automatically
supported.

## Client
Holds frontend code for public facing content. Within client there are
two distinct files `features` and `modules`. Features contains discrete features
that are sufficiently 'meaty'. e.g feature `react-slideshow` folder could contain
a react/flux single page app with it's own router.

Modules is for bog standard JavaScript libraries. e.g. module `login-form.js`, a
simply jquery library that attaches to a given login form partial.

Both Modules or Features are designed to be hooked up into the Webpack build.
To do this ensure the're included in `entry.js`. See `foo` examples for further info.

img - are images that are imported or referenced by the javascript/sass and build using webpack.
static - are discrete static images that are directly copied into the dist folder.   

## CMS
Holds legacy Admin Panel/CMS/Backend related javascript code.
Should be refactored at some point to reflect changes in Client.

## Getting started
To run the app, follow these steps.

1. Ensure that [NodeJS](http://nodejs.org/) is installed. This provides the platform on which the build tooling runs.
2. From the client folder, execute the following command:

```shell
	npm install
```

3. To start client hot-reloading and watch tasks run:

```shell
  npm start
```

or to pump out deploy ready assets:

```shell
  npm deploy
```

4. For cms legacy code run the following from the cms folder:

```shell
  grunt
```
