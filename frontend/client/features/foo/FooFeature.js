import './foo-style.scss';

export default class FooFeature {
    bar() {
        // defualt support for es6
        let hello = 10;
        console.log(`hello world I\m a self contained feature ${hello}`);
    }
}
