var webpack = require('webpack');
var WebpackDevServer = require('webpack-dev-server');
var config = require('./webpack.config.dev');

new WebpackDevServer(webpack(config), {
    https: true,
    publicPath: config.output.publicPath,
    hot: true,
    historyApiFallback: true
}).listen(1337, 'localhost', function(err) {
    if (err) {
        return console.log(err);
    }
    console.log('Listening at https://localhost:3000/');
});
