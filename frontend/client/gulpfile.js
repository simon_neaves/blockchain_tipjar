var gulp = require('gulp');
var watch = require('gulp-watch');

gulp.task('watch', function() {
    // Endless stream mode
    return watch('static/*.*', { ignoreInitial: false })
        .pipe(gulp.dest('dist'));
});
